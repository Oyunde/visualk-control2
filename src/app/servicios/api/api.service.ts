import { Injectable } from '@angular/core';
import {LoginI} from '../../models.login.interface';
import {ResponseI} from '../../models.response.interface';
import {HttpClient , HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs'
import { LoginI } from 'src/app/models/login.interface';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  url:string =  "https://office.visualk.cl:50346/b1s/v1/"
  constructor(private http:HttpClient) { }

  loginByUser(form:LoginI):Observable<ResponseI>{
    let direccion = this.url + "Login" ;
    return this.http.post<ResponseI>(direccion,form) ;
  }

}
