import { Component } from '@angular/core';
import { Socio } from './models/socio';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  

  socioArray: Socio[]= [  
    {FederalTaxID:"12-K" ,CardCode:1 ,CardName: "victor" , CardType: "C"},
    {FederalTaxID:"15-2" ,CardCode:2 ,CardName: "Alejandro" , CardType: "A"},
    {FederalTaxID:"9-1" ,CardCode:3 ,CardName: "Rojas" , CardType: "B"},

  ]; 

  selectedSocio: Socio = new Socio();

  openForEdit(socio: Socio) {
    this.selectedSocio = socio ;
  }

  addOrEdit(){

    if(this.selectedSocio.CardCode === 0){
      this.selectedSocio.CardCode = this.socioArray.length + 1;
      this.socioArray.push(this.selectedSocio);
    }
    this.selectedSocio = new Socio() ;
  }

  delete() {
    if(confirm('Estas seguro de querer eliminarlo')) {
      this.socioArray = this.socioArray.filter(x => x != this.selectedSocio);
      this.selectedSocio = new Socio() ;
    }
  
  }



}


